#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testIntegerToDecimal_A() {
	// Given
	const char* cadena ={3};
	// When
	int entero = integerToDecimal(3, cadena);
	// Then
    assertEquals_int(3, entero);
}

void testIntegerToDecimal_B() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToDecimal_C() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToDecimal_D() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToDecimal_E() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testDecimalToInteger_A() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testDecimalToInteger_B() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testDecimalToInteger_C() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testDecimalToInteger_D() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testDecimalToInteger_E() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToBinary_A() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToBinary_B() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToBinary_C() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToBinary_D() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToBinary_E() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testBinaryToInteger_A() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testBinaryToInteger_B() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testBinaryToInteger_C() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testBinaryToInteger_D() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testBinaryToInteger_E() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToOctal_A() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToOctal_B() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToOctal_C() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToOctal_D() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToOctal_E() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testOctalToInteger_A() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testOctalToInteger_B() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testOctalToInteger_C() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testOctalToInteger_D() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testOctalToInteger_E() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToHexadecimal_A() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToHexadecimal_B() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToHexadecimal_C() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToHexadecimal_D() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToHexadecimal_E() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testHexadecimalToInteger_A() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testHexadecimalToInteger_B() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testHexadecimalToInteger_C() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testHexadecimalToInteger_D() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testHexadecimalToInteger_E() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToNumericalBase_A() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToNumericalBase_B() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToNumericalBase_C() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToNumericalBase_D() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testIntegerToNumericalBase_E() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testNumericalBaseToInteger_A() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testNumericalBaseToInteger_B() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testNumericalBaseToInteger_C() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testNumericalBaseToInteger_D() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}

void testNumericalBaseToInteger_E() {
	// Given
	
	// When
	
	// Then
    fail("Not yet implemented");
}
