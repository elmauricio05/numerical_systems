#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jint JNICALL Java_library_binaryToInteger_1
  (JNIEnv *env, jobject object, jstring binary)
{
    javaEnv = env;
    const char* c_binary = toString(binary);
    int c_outValue = binaryToInteger(c_binary);
    return toJint(c_outValue);
}

JNIEXPORT jint JNICALL Java_library_decimalToInteger_1
  (JNIEnv *env, jobject object, jstring decimal)
{
    javaEnv = env;
    const char* c_decimal = toString(decimal);
    int c_outValue = decimalToInteger(c_decimal);
    return toJint(c_outValue);
}

JNIEXPORT jint JNICALL Java_library_hexadecimalToInteger_1
  (JNIEnv *env, jobject object, jstring hexadecimal)
{
    javaEnv = env;
    const char* c_hexadecimal = toString(hexadecimal);
    int c_outValue = hexadecimalToInteger(c_hexadecimal);
    return toJint(c_outValue);
}

JNIEXPORT jstring JNICALL Java_library_integerToBinary_1
  (JNIEnv *env, jobject object, jint integer)
{
    javaEnv = env;
    int c_integer = toInt(integer);
    const char* c_outValue = integerToBinary(c_integer);
    return toJstring(c_outValue);
}

JNIEXPORT jstring JNICALL Java_library_integerToDecimal_1
  (JNIEnv *env, jobject object, jint integer)
{
    javaEnv = env;
    int c_integer = toInt(integer);
    const char* c_outValue = integerToDecimal(c_integer);
    return toJstring(c_outValue);
}

JNIEXPORT jstring JNICALL Java_library_integerToHexadecimal_1
  (JNIEnv *env, jobject object, jint integer)
{
    javaEnv = env;
    int c_integer = toInt(integer);
    const char* c_outValue = integerToHexadecimal(c_integer);
    return toJstring(c_outValue);
}

JNIEXPORT jstring JNICALL Java_library_integerToNumericalBase_1
  (JNIEnv *env, jobject object, jint base, jint integer)
{
    javaEnv = env;
    int c_base = toInt(base);
    int c_integer = toInt(integer);
    const char* c_outValue = integerToNumericalBase(c_base, c_integer);
    return toJstring(c_outValue);
}

JNIEXPORT jstring JNICALL Java_library_integerToOctal_1
  (JNIEnv *env, jobject object, jint integer)
{
    javaEnv = env;
    int c_integer = toInt(integer);
    const char* c_outValue = integerToOctal(c_integer);
    return toJstring(c_outValue);
}

JNIEXPORT jint JNICALL Java_library_numericalBaseToInteger_1
  (JNIEnv *env, jobject object, jint base, jstring number)
{
    javaEnv = env;
    int c_base = toInt(base);
    const char* c_number = toString(number);
    int c_outValue = numericalBaseToInteger(c_base, c_number);
    return toJint(c_outValue);
}

JNIEXPORT jint JNICALL Java_library_octalToInteger_1
  (JNIEnv *env, jobject object, jstring octal)
{
    javaEnv = env;
    const char* c_octal = toString(octal);
    int c_outValue = octalToInteger(c_octal);
    return toJint(c_outValue);
}

