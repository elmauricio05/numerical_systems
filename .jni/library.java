/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "binaryToInteger(const char*):int")
    public int binaryToInteger(String binary){
        return binaryToInteger_(binary);
    }
    private native int binaryToInteger_(String binary);

    @executerpane.MethodAnnotation(signature = "decimalToInteger(const char*):int")
    public int decimalToInteger(String decimal){
        return decimalToInteger_(decimal);
    }
    private native int decimalToInteger_(String decimal);

    @executerpane.MethodAnnotation(signature = "hexadecimalToInteger(const char*):int")
    public int hexadecimalToInteger(String hexadecimal){
        return hexadecimalToInteger_(hexadecimal);
    }
    private native int hexadecimalToInteger_(String hexadecimal);

    @executerpane.MethodAnnotation(signature = "integerToBinary(int):const char*")
    public String integerToBinary(int integer){
        return integerToBinary_(integer);
    }
    private native String integerToBinary_(int integer);

    @executerpane.MethodAnnotation(signature = "integerToDecimal(int):const char*")
    public String integerToDecimal(int integer){
        return integerToDecimal_(integer);
    }
    private native String integerToDecimal_(int integer);

    @executerpane.MethodAnnotation(signature = "integerToHexadecimal(int):const char*")
    public String integerToHexadecimal(int integer){
        return integerToHexadecimal_(integer);
    }
    private native String integerToHexadecimal_(int integer);

    @executerpane.MethodAnnotation(signature = "integerToNumericalBase(int,int):const char*")
    public String integerToNumericalBase(int base, int integer){
        return integerToNumericalBase_(base, integer);
    }
    private native String integerToNumericalBase_(int base, int integer);

    @executerpane.MethodAnnotation(signature = "integerToOctal(int):const char*")
    public String integerToOctal(int integer){
        return integerToOctal_(integer);
    }
    private native String integerToOctal_(int integer);

    @executerpane.MethodAnnotation(signature = "numericalBaseToInteger(int,const char*):int")
    public int numericalBaseToInteger(int base, String number){
        return numericalBaseToInteger_(base, number);
    }
    private native int numericalBaseToInteger_(int base, String number);

    @executerpane.MethodAnnotation(signature = "octalToInteger(const char*):int")
    public int octalToInteger(String octal){
        return octalToInteger_(octal);
    }
    private native int octalToInteger_(String octal);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
