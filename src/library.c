#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

const char* integerToDecimal(int integer) {
    return "-1000";
}

int decimalToInteger(const char* decimal) {
    return -1000;
}

const char* integerToBinary(int integer) {
    return "-1000";
}

int binaryToInteger(const char* binary) {
    return -1000;
}

const char* integerToOctal(int integer) {
    return "-1000";
}

int octalToInteger(const char* octal) {
    return -1000;
}

const char* integerToHexadecimal(int integer) {
    return "-1000";
}

int hexadecimalToInteger(const char* hexadecimal) {
    return -1000;
}

const char* integerToNumericalBase(int base, int integer) {
    return "-1000";
}

int numericalBaseToInteger(int base, const char* number) {
    return -1000;
}
